# Wayland-idfix

The work here is a result of the discussion:

https://lists.freedesktop.org/archives/wayland-devel/2023-September/043060.html

with some changes and additions.

tl;dr: clients and servers can retain type info for all IDs until they
determine that the other side won't send messages to those IDs.

New environment variables:

* WAYLAND_MAX_ZOMBIE_LIST_COUNT
  defaults to 64

  controls the number of deleted IDs that the server will retain as
  "zombies" (only their interfaces remain) before reusing them in LRU
  fashion.  Setting to 0 disables the zombie list, and simulates the
  old MRU behavior without using zombies for server ID management in
  the server.  A non-0 setting is most useful for servers that may
  encounter unpatched clients that cannot send delete_id requests (or
  which are patched, but have WAYLAND_SERVER_DELETE_ID_HANDSHAKE set
  to false).  A non-0 setting does not impact execution otherwise.

* WAYLAND_SERVER_DELETE_ID_HANDSHAKE
  defaults to true

  if set to "0", "no" or "false", will prevent the server from sending
  an initial delete_id event with id=0xffffffff.  A patched client
  that does not receive this handshake will not subsequently send
  delete_id requests using sync requests.  Using sync requests as
  delete_id requests is admittedly very kludgey, and should get
  replaced eventually by adding an actual delete_id request to the
  protocol.  However, unlike new protocol, this kludge works for
  existing clients and servers that use a patched libwayland.

* WAYLAND_SERVER_ZOMBIE_DELETE_ID
  defaults to true

  if set to "0", "no" or "false", prevents the server from sending
  delete_id events corresponding to new_id args in requests sent to
  server ID zombies.  Leaving it or setting it otherwise allows for
  handling the "domino effect" zombie cases without leaking IDs, as
  explained below.

The above environment variables all default to the new behavior.


Motivation:

Currently, libwayland has some bugs related to the lifetimes and reuse
of object IDs.  In order to properly decode wire format messages,
libwayland for both compositors and clients has to maintain an
accurate mapping from object IDs to interfaces (object types).  If a
message arrives on the wire and the receiver cannot accurately match
the object ID in the message header with an interface, libwayland will
react either by severing the connection (killing the client), or
(rarely) by experiencing unpredictable behavior.  The preference would
be severing the connection, but the problem is not always detected.
The reason to sever the conneciton is because subsequent messages in
the wire can only be understood if all prior messages in that
direction were properly decoded.

The reason that a message might arrive without the receiver having the
proper target object ID to interface mapping is that object deletion
can occur one one side (compositor vs. client) asynchronously with
usage of the same object by the other side.  The current libwayland
code has the ability to properly handle some of these cases, but not
all.  In some of those cases, the object ID can be reused before the
other side is aware of the original deletion.

The features of current libwayland that allow it to properly handle
some cases of asynchronous object deletion are the
wl_display::delete_id event, and "zombies", which are placeholders
that prevent reuse of a deleted client object ID and retain some
interface information (but not enough) to decode events sent to that
ID.

An important case that is not addressed by either
wl_display::delete_id or zombies is when an object is created by the
compositor and subsequentely deleted by the compositor.  Then the same
object ID is used again by the compositor to create a second object.
When the client sends a request involving the ID shared by those two
objects, libwayland in the compositor does not know which one the ID
refers to.  If the two objects had different interfaces, then
libwayland in the compositor can determine if the message refers to
the newer object and only accept it then - but it severs the
connection with the client (killing the client) otherwise.  If the two
objects share the same interface, then the compositor does not detect
the problem, and if the request really involved the older object, some
unpredictable behavior may occur.

There are also unhandled cases in the other direction: when an object
is created by the client and subsequently deleted by the client.  This
case is mostly handled by "zombies" - which are placeholders in the
client that keep the object ID reserved (not reusable for new objects)
until the compositor sends a wl_display::delete_id event for that ID.
However, if the compositor sends an event to that ID prior to
registring the deletion and sending the wl_display::delete_id event,
and that sent event contains "new_id" arguments signifying the
creation of other compositor objects and specifying their interfaces,
the zombie mechanism does not allow the client to register these other
new ID -> interface mappings for use in decoding future events that
the compositor might send, still prior to registering the deletion and
sending the wl_display::delete_id event.  If the compositor sends an
such an event, involving one of the IDs mentioned as new_IDs in that
first event, the client will sever the connection (and die).  These
events with new_ids can be followed by events targeting those new_ids
with other new_ids, and this can continue in a "domino effect".

There are other cases as well, involving objects created on one side
and delete on the other.

Goals: [Note: "patched libwayland" referes to a libwayland containing
the changes proposed here]

1. maintain compatibility with existing compositors and clients, even
if they are not using a patched verison of libwayland.

2. when both compositor and client use a patched libwayland, handle
all cases of messages sent to deleted objects, including "domino
effect" cases of messages sent to objects created by new_id arguments
of messages sent to deleted objects, messages sent to objects created
by new_id arguments to those messages, and so on, without severing the
compositor/client connection or experiencing unpredictable behavior.
Do not leak object IDs (leak = make some not reusable).

3. when only the compositor is using a patched libwayland, reduce the
likelihood of compositor object ID reuse bugs, and handle cases of
requests sent to client objects that were deleted by the server and
their "domino effects".  Some ID leaking is permitted.

4. when only the client is using a patched libwayland, properly handle
the cases of client object ID reuse, including "domino effect" cases.
Also handle cases of events sent to compositor objects that were
deleted by the client and their "domino effects".  Some ID leaking is
permitted.

5. Do not slow down the protocol.  Do not significantly increase
resource requirements.  Do not alter the protocol specification.  Do
not alter the wire format.

6. (my own benefit, hopefully others as well) Add a few non-static
entry points for middleware usage.  But don't burden either clients or
compositors with middleware requirements.  (By middleware I mean:
utilities that act as both client and server, they speak to a
compositor through one socket and to their own set of clients through
another).  These extra entry points are motivated by the prior need
for middleware projects to copy the libwayland code into their own
sources and modify it mostly just to add these entry points, instead
of linking to an existing libwayland.

Proposed changes:

The proposed changes worked on here involve replacing the "zombie"
mechanism with one that can handle the potential "domino effect" of
new_ids from messages targeted at deleted objects, using that for both
clients and compositors, and adding a request equivalent to the
delete_id event so that it can be used in both directions.  Also
included is a modification of the greedy object ID reuse algorithm,
changing it from MRU to LRU (from LIFO to FIFO) so that if only one
side (client or compositor) is running libwayland with these patches,
there is still an ability to lower the probability of the above bugs.

It has been noted previously
(https://gitlab.freedesktop.org/wayland/wayland/-/issues/100) that the
Wayland protocol would benefit from a request (client-to-compositor)
equivalent to the existing wl_display::delete_id message.  These
changes include such a request done in a compatible fashion so that
patched clients only use it with patched compositors that have
signalled that they understand it.

* Have a patched libwayland compositor send at an early (before the
  first event containing a new_id argument) a wl_display::delete_id
  with an impossible ID (like 0xffffffff) to the client.  If the
  client is also using a patched libwayland, it will record that it
  has received this event as proof that the compositor is patched and
  can receive delete_id requests.  A client that is not using a
  patched libwayland will log the impossible wl_display::delete_id
  event, but will otherwise continue.

* Have patched clients send a specially coded wl_display::sync request
  (with server ID arg instead of client ID arg) that acts as a delete_id
  request to patched compositors (which they detect as above) when the
  client deletes a server-side object (in response to a destructor
  event).  The client should also send such a request immediately
  after receiving the impossible ID wl_display::delete_id event above,
  so that the server can prepare early to receive subsequent events of
  this type.

* Instead of separate zombie objects that contain only part of the
  interface information needed to decode messages, have deleted ID
  entries in the wl_maps contain pointers to the wl_interfaces, which
  have all of the necessary information, including the types of new_id
  arguments.

* When a message is decoded that targets a deleted object, properly
  handle both its fd freight and any new_id arguments.  The new_id
  arguments are handled by adding them as deleted objects and sending
  a delete_id message (if the other side can handle it) for each new
  ID.  The messages targeting deleted objects are logged and dropped
  after being decoded and having their fds and new_ids managed.

* Unify the deleted object ID maintenance for both compositor and
  client objects IDs on both compositor and client sides of
  libwayland.  In the case where the other side can send delete_id
  messages (true for all compositors and for patched clients), deleted
  IDs are not reused until a delete_id message is received from the
  other side.

* Move the marking of objects and zombies as having been the argument
  in a delete_id message into the wl_map code.

* Add a LRU/FIFO zombie list to make the ID reuse algorithm less
  greedy, for the benefit of cases where only one side is running a
  patched libwayland.  Allow the max size of the list to be settable
  using an environment variable, including setting it to 0 to
  reproduce the current greedy algorithm.

* Those middleware-benefiting entry points (formerly static functions
  in libwayland).



From upstream:

# Wayland

Wayland is a project to define a protocol for a compositor to talk to
its clients as well as a library implementation of the protocol.  The
compositor can be a standalone display server running on Linux kernel
modesetting and evdev input devices, an X application, or a wayland
client itself.  The clients can be traditional applications, X servers
(rootless or fullscreen) or other display servers.

The wayland protocol is essentially only about input handling and
buffer management.  The compositor receives input events and forwards
them to the relevant client.  The clients creates buffers and renders
into them and notifies the compositor when it needs to redraw.  The
protocol also handles drag and drop, selections, window management and
other interactions that must go through the compositor.  However, the
protocol does not handle rendering, which is one of the features that
makes wayland so simple.  All clients are expected to handle rendering
themselves, typically through cairo or OpenGL.

Building the wayland libraries is fairly simple, aside from libffi,
they don't have many dependencies:

    $ git clone https://gitlab.freedesktop.org/wayland/wayland
    $ cd wayland
    $ meson build/ --prefix=PREFIX
    $ ninja -C build/ install

where PREFIX is where you want to install the libraries.

See https://wayland.freedesktop.org for documentation.
